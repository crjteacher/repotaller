// Para gestionar productos
function getProducts() {
  var url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Products';
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var productosObtenidos = request.responseText;
      procesarProductos(productosObtenidos);
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos(productsAsString) {
    var productsAsJson = JSON.parse(productsAsString);
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    fillProductTableHeaders(tabla);
    for (var i = 0; i < productsAsJson.value.length; i++) {
      var row = document.createElement("tr");
      var columnName = document.createElement("td");
      var columnPrice = document.createElement("td");
      var columnStock = document.createElement("td");
      columnName.innerText = productsAsJson.value[i].ProductName;
      columnPrice.innerText = productsAsJson.value[i].UnitPrice;
      columnStock.innerText = productsAsJson.value[i].UnitsInStock;
      row.append(columnName);
      row.append(columnPrice);
      row.append(columnStock);
      tbody.appendChild(row);
    }
    tabla.appendChild(tbody);
    divTabla.append(tabla);
}

function fillProductTableHeaders(table) {
  var headerName = document.createElement("th");
  headerName.innerText = "Nombre";
  var headerPrice = document.createElement("th");
  headerPrice.innerText = "Precio";
  var headerStock = document.createElement("th");
  headerStock.innerText = "Unidades";
  table.append(headerName);
  table.append(headerPrice);
  table.append(headerStock);
}
