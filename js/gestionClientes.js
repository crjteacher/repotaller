function getCustomers() {
  var url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Customers';
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      procesarClientes(request.responseText);
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(clientsAsString) {
    var clientsAsJson = JSON.parse(clientsAsString);
    var divClientes = document.getElementById("divClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    fillClientTableHeaders(tabla);
  for (var i = 0; i < clientsAsJson.value.length; i++) {
      var row = document.createElement("tr");
      var columnName = document.createElement("td");
      var columnCity = document.createElement("td");
      var columnPhone = document.createElement("td");
      var columnCountry = document.createElement("td");
      columnName.innerText = clientsAsJson.value[i].ContactName;
      columnCity.innerText = clientsAsJson.value[i].City;
      columnPhone.innerText = clientsAsJson.value[i].Phone;
      var imgFlag = document.createElement("img");
      imgFlag.classList.add("flag");
      imgFlag.src = getCountryFlag(clientsAsJson.value[i].Country);
      columnCountry.appendChild(imgFlag);
      row.append(columnName);
      row.append(columnCity);
      row.append(columnPhone);
      row.append(columnCountry);
      tbody.appendChild(row);
    }
    tabla.appendChild(tbody);
    divClientes.append(tabla);
}

function getCountryFlag(country) {
  if (country == 'UK') {
    country = 'United-Kingdom';
  }
  return flagUrl = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + country + ".png";
}

function fillClientTableHeaders(table) {
  var headerName = document.createElement("th");
  headerName.innerText = "Nombre";
  var headerCity = document.createElement("th");
  headerCity.innerText = "Ciudad";
  var headerPhone = document.createElement("th");
  headerPhone.innerText = "Teléfono";
  var headerCountry = document.createElement("th");
  headerCountry.innerText = "País";
  table.append(headerName);
  table.append(headerCity);
  table.append(headerPhone);
  table.append(headerCountry);
}
